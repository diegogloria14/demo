/// <reference types="cypress" />

it('Search for ISBN with Content Approved status', () => {
    //set viewport of browser
    cy.viewport('macbook-15');
    //visit Cambridge Core, type mathematics, then click on the search button
    cy.visit('https://content-admin-si.aop.cambridge.org/');
    //type credentials
    cy.get('[placeholder="Username"]').type('dgloria@cambridge.org');
    cy.get('[placeholder="Password"]').type('Password1');
    //click login button
    cy.get('.login-button > .button').click();
    //click Content Admin tab
    cy.get('[href="#panelcontent-admin"]').click();
    //click Books under Content
    cy.get(':nth-child(3) > ul > :nth-child(1) > a').click();
    //Assert that page is redirected to Books
    cy.get('.breadcrumbs > .current > a').should('have.text', 'Books');
    //type ISBN and search
    cy.get('label > input').type('9781108590570{enter}');
    //select status of book
    cy.get('#statusFilter').select('Content-Approved');


    


   })
   